package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Teste the constructor")
    public void testTheConstructor() {
        //Arrange
        String firstName= "Wolverine";
        String lastName= "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        //Act
        Employee wolverine = new Employee(firstName, lastName, description, jobTitle);
        //Assert
        assertEquals(firstName, wolverine.getFirstName());
        assertEquals(lastName, wolverine.getLastName());
        assertEquals(description, wolverine.getDescription());
        assertEquals(jobTitle, wolverine.getJobTitle());
    }

}