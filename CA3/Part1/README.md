## Class Assignment 3
### Report working with Virtual Box

In order to solve the CA3 - Part1, i firstly need to download the Virtual Box from their [Homepage](https://www.virtualbox.org/).

After it i need to install it on my laptop, and only after it i could create a new virtual machine.

On the VirtualBox and in order to create a new Virtual Machine, i need to choose the option "Novo", as it is demonstrated
 by the image below:

![](./Images/InstallingVM_0.png)

After it, i should choose the name that i'd like to give to my Virtual Machine, as well as the OS, and the version that I 
would like to install, as we can acknowledge by the next image:

![](./Images/InstallingVM_1.png)

Next step is to choose the RAM memory that i'd like to allocate to this VM.
Which in this case i selected to create a VM with only 2048 MB, as it is represented with the image below:

![](./Images/InstallingVM_2.png)

Now i need to select the size of the virtual hard disk, which in this case i simply accepted the 10 GB, and create a new virtual hard disk, 
as it is possible to acknowledge in the image below:

![](./Images/InstallingVM_3.png)

In this step, we can choose the type of virtual hard disk that i'd like to create. I accepted the default option, as represented by the following 
image:

![](./Images/InstallingVM_4.png)

In this step we have the opportunity to choose if i'd like to allocate the 10 GB, or if i'd like to dinamically allocate hard disk 
as i need it, until a maximum of 10 GB.
The option was to choose the option of dynamically allocated, as illustrated by the image that follows: 


![](./Images/InstallingVM_5.png)

The last step is to select the size of the disk that i'd like to allocated to this VM, and the location on my laptop of
 this allocation.
 
![](./Images/InstallingVM_6.png)

Now I need to set up the network.
First of all i need to create a new VirtualBox network as it is demonstrated by the image below:

![](./Images/InstallingVM_7.png)

Now i need to go to "Configurações", in order to configure the network.

![](./Images/InstallingVM_0.png)

The first adapter is automatically configurated, and is main function is to able the VM to have access to the internet.

![](./Images/InstallingVM_8.png)

The second adapter i needed to configure it as a host-only adapter, which is similar to a network card that enables the
 connection between all the VM, as well as between the VM to the host OS.
 
 I configured it as demonstrated by the image below:

![](./Images/InstallingVM_9.png)


After setting up the network I need to download an ISO file with the Ubuntu in order to install this OS in the VM.

To do so, i downloaded the file from the [Ubuntu website](https://help.ubuntu.com/community/Installation/MinimalCD).

Now in order to install the downloaded ISO file i need to put it on the "Armazenamento", as it is possible to acknowledge
 by the image below:

![](./Images/InstallingVM_10.png)

After it i only need to left-click on the "Iniciar" button, to start the installation of the OS in the VM. 

![](./Images/InstallingVM_0.png)

Finished the installation it is time to make several installations in order to finished the set up of this VM,
regarding not only the network, but also SSH, GIT, JDK and so on.

In order to test if everything is as planned, i can do 2 things.

First of all i need to check if the connection between my localhost and the VM is established, and to do so i need 
to ping the ip address 192.168.56.100, using the command ``ping 192.168.56.100``. And as it can be acknowledge the 
connection has been established:

![](./Images/VerifyingConnection.png)

The next step is to check if the host-only adapter is set up the right way.
To do so, i needed to check if it is possible to established a SSH connection between my localhost and the VM, 
using for this test the command ``ssh matos@192.168.56.100``.
And as we can see through the image below, i was capable of connecting my localhost to the VM:

![](./Images/VerifyingConnection2.png)

#### Task 2 of the Ca3/Part1

After setting up the VM, i needed to create a new folder to where i can clone my repository. 
And for it i needed to write in the command line the following lines of code:

**1 -** Create a new directory:
```Batch
mkdir devops-19-20-A-1121016
```
**2 -** Clone the repository to my VM hard disk:
```Batch
git clone https://AlaMatos@bitbucket.org/AlaMatos/devops-19-20-a-1121016.git
```
#### Task 3 of the Ca3/Part1

For this task i'm going to execute the spring-boot of the CA1 assignment.
And for it i need to be on the basic directory within the CA1. And verify if the mvnw as permission for execution, which
in this case it doesn't, as it is represented by the image below:

![](./Images/task3_1.png)

In order to solve this issue i needed to change permissions of the file, and to do so i typed the command ``chmod mvnw +x``,
 after it i verified that the file has now executable permissions, which can be analyzed by the image below.
 
 ![](./Images/task3_2.png)
 
#### Task 4 of the Ca3/Part1

 Now i can build the maven project and run the framework spring-boot, typing the command ``./mvnw spring-boot:run``.
 
 As we can acknowledge by the image below, I successfully built and execute the spring-boot project CA1. 
 
 ![](./Images/task3_3.png)
 
#### Task 5 of the Ca3/Part1
 
 Regarding the gradle_basic_demo project, it is fundamental to have in mind that my VM hasn't any desktop, 
 so the window of the client side won't appear if i run the task in the VM.
 In order to solve this problem i need to start the server on my VM, and run the task **runClient** on my laptop.
 
 So first of all i need to be on the directory that i have the gradle project.
 
 And after it i need to verify if the gradlew as permission for execution, which in this case it doesn't, 
 as it is represented by the image below:
 
 ![](./Images/task5_1.png)
 
 In order to solve this issue i needed to change permissions of the file, and to do so i type the command ``chmod gradlew +x``,
  after it i verified if the file has now executable permissions, which can be analyzed by the image below.
  
  ![](./Images/task5_2.png)
  
Now i can build the gradle project, typing the command ``sudo ./gradlew build``.
   
As we can acknowledge by the image below, I successfully built the gradle project. 
   
   ![](./Images/task5_3.png)
   
   Next step is to run the server in the VM side. To do so i need to type the command ``sudo ./gradlew runserver`` in order to run the server, 
as it can be acknowledge by the image below:
 
 ![](./Images/task5_4.png)
 
 Regarding the task runClient, this must be done on my localhost, in the directory of the gradle project.

But before running the task mentioned, i need to make some changes on the task itself. In other words, i need to change 
the args ``LocalHost`` to ``192.168.56.100``, as it can be seen in the image below:


```Gradle
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.100', '59001'
}
```
This change must be done, otherwise i'd have a connection error as it can be seen in the image below:
 
 ![](./Images/task5_5.png)
 
 The reason for this change is due to the fact that the server is located in 192.168.56.100, so if i want to connect a client
 to the server, i need to inform the task where to connect, and in what port.
