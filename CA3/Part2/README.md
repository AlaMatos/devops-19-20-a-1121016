## Class Assignment 3/Part2
### Report working with Virtual Box

In order to solve the CA3 - Part2, i first need to create a new folder where i can add all the files needed to solve this
part of the assignment.

And to do so i needed to be on the right directory in order to write in the command line the following lines of code:

**1 -** Create a new directory:

```Batch
mkdir Part2
```

The next step was to add a vagrantFile from [this repository](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/), 
into the folder that i previously created.

The main objective is to build and manage two virtual machines, a **web application** and a **database**.

The **database** virtual machine executes the H2 database. And the **web application**, which executes inside Tomcat8.

So, before starting the DB and WEB virtual machine, i needed to make some changes on my repository.

The project that i want to launch to the virtual machines is the CA2/Part2.
In order to manage it successfully, i firstly need to make some changes on my CA2/Part2 project using the guidelines of 
[this repository](https://bitbucket.org/atb/tut-basic-gradle).
In order to make it easy to analyze I'll enumerate all the changes that I made:

**1 -** Changed the build.gradle file in order to support war file building, as it can be acknowledge by the image below:

![](./Images/Change_BuildGradle.PNG)

**2 -** Fixed the reference in index.html to the css file, as it is shown by the image below:

![](./Images/Index_Html.PNG)

**3 -** Created a class ServletInitializer, as it can be seen by the following image:

![](./Images/ServletInitializer.PNG)

Needed to add one more change because i missed the extends SpringBootServletInitializer part, as it is represented by the image below:

![](./Images/ServletInitializer2.PNG)

**4 -** Regarding the application context path, needed to make a small change regarding the name of the war.file created 
after building my gradle project. To be sure that the file name was the correct one, i checked the file in the directory build/libs 
of my project CA2/Part2 on my local repository.

The changes made are ilustrated by the image below:

![](./Images/App_js.PNG)
       
**5 -** The major changes consisted in adding support for H2 console, as enable spring in order not to drop de 
database on every execution, as well as applying settings for remote h2 database.

![](./Images/Application_Properties.PNG) 

Regarding the change made on the previous point, I also needed to change the application context path due to the fact that 
my war file is named demo, instead of basic, as it can be acknowledge by the image below:

![](./Images/Application_Properties_2.PNG)

**6 -** After this major changes i needed to change the repository on the vagrant file, in order to clone my repository.

To do so i needed to make the following changes which are shown in the image below:

![](./Images/Clone.PNG)


After all these changes i needed to start both virtual machines, and to do so i needed to type the following command line:
 
```Batch
vagrant up
```

After that I could verify if both machines are up and running. To do so i must type the following command line:

```Batch
vagrant status
```

Giving me the confirmation that both machines are running, as we can see through the next image:

![](./Images/VagrantStatus.PNG)

I also used the following command line, when i wanted to reload my virtual machines with some new changes that i added.
 

```Batch
vagrant reload --provision
```

Regarding the vagrantfile, and in order to make my gradle project running on the virtual machines i needed to make 
several changes, which I'll be describing them, as follows:

Firstly, it appeared me an error in npm (as it is shown below), and I decided to delete the directories node,
 node_modules and build. And after i pushed all these changes to my bitbucket repository.
  
![](./Images/error_to delete node.PNG)

Regarding the vagrantFile, i made some changes regarding the software that i would like to install in each machine when 
they are built. 

In the DB machine i choose to install the vim program, as follows:

![](./Images/DB_vim.PNG)

In terms of the WEB machine, i decided to install nano, as follows:

![](./Images/WEB_nano.PNG)

I decided as well to delete the directory ``devops-19-20-A-1121016`` in order to avoid any kind of errors when 
building my WEB machine, as it can be checked by the image below:

![](./Images/VagrantFile.PNG)


After building successfully the DB and WEB virtual machine, i needed to check if both virtual machines are up and running.

To do so i needed to use on the browser the URL ``http://localhost:8080/demo-0.0.1-SNAPSHOT/``, in order to check if the
spring web application is running, as it can be acknowledge by the image below:

![](./Images/WEB.PNG)

Regarding the test of the database i needed to use on the browser the URL ``http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console``, 
in order to open the H2 console, as it can be acknowledge by the image below:

![](./Images/H2_DB_Testing.PNG)
![](./Images/H2_DB.PNG)

## Analysis of an Alternative / Hyper-V

###Theoretical contextualization

Hyper-v as well as Virtual box are software's that allows the user to run one or more virtual machines with 
their own OS on a physical host machine. 

This type of software is called Hypervisor. And grabbing these two exampes we have
an hypervisor type 1 (Hyper-V) and an hypervisor type 2 (Virtual Box).

The Hyper-V is a different hypervisor type when compared to Virtual Box, due to the fact that it runs directly on the 
host computer hardware. So when a host computer is turned on, the Hyper-V starts with him and starts managing
the OS. If the user has the right settings it'd be able to start VM automatically. 

In terms of Virtual Box, is an application that runs on the host OS, which needs to be installed previously in the host computer.
In this case, when a host computer is turned on, Virtual Box doesn't start until the user starts the Virtual Box application
and starts the needed VM, thus creating hosted processes.

In other words, the Hyper-V is always on if the host is powered on, on the contrary VirtualBox can be started and closed 
by a user on demand.

___
### Implementation of Hyper-V

The main reason to use Hyper-V was the fact that this virtualization tool is a functionality of the windows 10 itself.

Despie, all the comments that i read regarding how to install/activate Hyper-V in a Windows 10 Home, would lead 
always to the same result. Windows 10 home, doesn't have access to Hyper-V.

Nevertheless i continued my research and found a solution to make Hyper-V to work in a Windows 10 Home.

To so so, i needed to run the ``HYPER-V_Package_Installer.bat`` file as administrator and restart my pc.

For this kind of report i'll be only referring the source of that information (check the link below), and add the referred 
file to the repository.

+ https://github.com/MicrosoftDocs/Virtualization-Documentation/issues/915

After following the guidelines given in the previously source i was able to activate Hyper-V on my machine, as it can 
be acknowledged by the image below:

![](./Images/HyperV.png)


In order to solve the CA3 - Part2_Alternative, i first need to create a new folder where i can add the project, which in 
this case i need to make some changes in order to make it run using Hyper-V. The main changes are concerning the vagrant file.


To do so i needed to be on the right directory in order to write in the command line as follows:

**1 -** Create a new directory called ``Part2_Alternative``, where it'll be the location of the vagrant file:

```Batch
mkdir Part2_Alternative
```
**2 -** And after it i created another new directory called ``HyperV``:

```Batch
mkdir HyperV
```

The next step was to copy the gradle project from CA2/Part2, into the folder ``HyperV`` that i previously created.

Having in mind that the gradle project CA2/Part2 is running i only need to make minor changes, as i'm going to explain next.

In order to make it easy to analyze I'll enumerate all the changes that I made:

**1 -** Firstly I need to choose a Vagrant box to use with Hyper-V, and to do so I chose this 
[generic/ubuntu1604 box](https://app.vagrantup.com/generic/boxes/ubuntu1604).


**2 -** After it I needed to change the vagrant file according with the new provider. In other words, I needed to 
change the configuration of the VM from ``envimation/ubuntu-xenial`` to ``generic/ubuntu1604``, as it can be acknowledged 
by the image below:

![](./Images/Vagrant_HyperV.PNG)

In terms of Vagrant file, I needed to make the following changes:

#####Local configuration

```Batch
Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1604"
  config.vm.provider "hyperv"
```
#####DB configuration

```Batch
config.vm.define "db" do |db|
    db.vm.box = "generic/ubuntu1604"
```
#####WEB configuration

```Batch
config.vm.define "web" do |web|
    web.vm.box = "generic/ubuntu1604"
```

In terms of consistency i decided to define both VM with the same RAM memory. To do so i needed to add the following 
command in the vagrant file:

```Batch
# We set more ram memmory for this VM
 db.vm.provider "hyperv" do |v|
 v.memory = 1024
 end
```

And once that i've changed the directory of the gradle project that I'll be using for this assignment, 
i need to change the path that i previously had into the following:

```Batch
cd devops-19-20-a-1121016/CA3/Part2_Alternative/HyperV
```

**3 -** Next step is to install the vagrant box that enables me to work with vagrant and Hyper-V. And to do so, i need 
to type the following command line:

``vagrant box add generic/ubuntu1804 --provider hyperv``  

**4 -** Now in order to start my virtual machines, i typed the following command:

``vagrant up --provider hyperv``

**5 -** In order to check if both machines are up and running i decided to enter in both machines.
First i entered in the DB machine, in order to check the IP address, that i'll need to use in the web machine.

So to do so i needed to type the following command:

```Batch 
vagrant ssh db
```

Followed by the command ``ifconfig``, which returned me the IP address that i needed, as the following image illustrates:

![](./Images/Vagrant_HyperV_DB.PNG)

**6 -** Now i need to update the IP address on the application.properties file that i have in the WEB machine.
To do so, firstly i needed to access to the WEB machine, by typing the following command:
 
```Batch 
vagrant ssh web
```

**7 -** To update the IP address on the application.properties file i could change it manually as it can be acknowledged by the image below:

![](./Images/Vagrant_HyperV_WEB.PNG)

+ After it i must build the whole project, and to do so i must be in the directory of the gradle project that i want to build, 
and only after it i can type the following command to build the project:

```Batch
sudo ./gradlew clean build
```

+ The next needed step is to copy the new build file ``demo-0.0.1-SNAPSHOT.war`` into the tomcat8 server. 
To do so i needed to type the following command:

```Batch
sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps 
```

After all these steps it is time to test if our virtual machines are fully configured. 
 To do so I needed to check if both virtual machines are up and running.

So i needed to use on the browser the URL ``http://172.18.63.66:8080/demo-0.0.1-SNAPSHOT/``, in order to check if the
spring web application is running, as it can be acknowledge by the image below:

![](./Images/Vagrant_HyperV_WEB_.PNG)

Regarding the test of the database i needed to use on the browser the URL ``http://172.18.63.68:8082``.

In order to access the H2 console, i needed to type the a new **JDBC URL** which in this case is ``jdbc:h2:tcp://172.18.63.68:9092/./jpadb``. 
This step can be acknowledge by the image below:

![](./Images/Vagrant_HyperV_DB_1.PNG)

After access the console H2, I could check that the database has all the fields that the gradle project had. 

As it can be acknowledge by the image below:

![](./Images/Vagrant_HyperV_DB_2.PNG)

















































































































