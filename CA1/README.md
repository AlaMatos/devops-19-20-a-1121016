## Class Assignment 1
### Report working with GIT using the command line

###### In the image below it is possible to acknowledge all the commits done during this assignment.
 
![](.\images\commits.png)

First of all I created a repository on Bitbucket, and after i've cloned it through the command line in the folder 
that I wanted to save my work.

In order to clone the repository created i typed in the command line, the following command:

``` Batch
git clone https://AlaMatos@bitbucket.org/AlaMatos/devops-19-20-a-1121016.git
```
After pasting the folder CA0 on the folder repository, I needed to add this changes to the hosted repository on Bitbucket.

In order to do so i needed add all the files.
``` Batch
git add .
```
After it, I had to commit all the changes referring the issue previously created on Bitbucket, and add a simple message.
``` Batch
git commit -am "ref #1 - Initial version"
```
Now i could push all the changes made in my repository.

``` Batch
git push
```

In order to add a tag to the previous push i created a tag adding to it a message and after it i pushed to the repository, 
as it is possible to acknowledge in the image below.

![](.\images\InitialCommit.png)

``` Batch
git tag -a v1.2.0 -m "Initial version"
git push origin v1.2.0
```

In order to add the email-field in the application we decided to create a branch. To do so, firstly I needed to check the list of 
all the branches (just to be sure that it was empty).

``` Batch
git branch
```

And after it i created a branch which I named "email-field".

``` Batch
git branch email-field
```

Next step was to change branches in order i could be able to work on it (changed from origin to email-field).

``` Batch
git checkout email-field
```

After making all the changes desired I wanted to make my first push of the branch email-field, to do so I
 needed to commit all the changes and only after it i could push it into the repository. Giving us the commit below:
 
 ![](.\images\SecondCommit.png)
``` Batch
git commit -am "ref #2 - Add a new feature (email-field)"
```
``` Batch
git push --set-upstream origin email-field
```

After committing some more changes in the master branch, I needed to merge this branch with the **email-field** branch.

To do so I needed to firstly check if i was already in the master branch by typing `git branch`. Which i already was.

Nevertheless, if i wasn't on the master branch i should checkout to it, typing the command below.
``` Batch
git checkout master
```
I can now proceed with the merge operation with the **email-field** branch.
``` Batch
git merge email-field
```
After merging the branches I need to push all the modifications into the repository.
``` Batch
git push
```
Now i would like to tag this last push, and for it I created a new tag and pushed it into the origin, has it can be seen
in the image below:

 ![](.\images\FirstMerge.png)
``` Batch
git tag -a v1.3.2 -m "Minor updates of the v1.3.1"

git push origin v1.3.2
```

In order to solve the issue of the invalid email I created a new branch which I named "**fix-invalid-email**".
``` Batch
git branch fix-invalid-email
```
In order to be able to work on it i needed to change branches (**from origin to fix-invalid-email**). 
If I wasn't certain regarding the branch that I was in I could insert the command `git branch` in order to check where the HEAD is.

``` Batch
git checkout fix-invalid-email
```

After making all the changes needed to solve the issue referred previously i needed to make my first push of
 the branch **fix-invalid-email**, as it can be seen in the image below.
 
  ![](.\images\FirstCommit_FixInvalidEmail.png)

``` Batch
git commit -am "Ref #3 - Fixing a bug in the e-mail field"

git push --set-upstream origin fix-invalid-email
```

After testing if everything is working just fine, I needed to merge the master branch with the fix-invalid-email branch.

To do so, and having in mind what was said before, I need to be in the master branch, that's the reason why I needed 
to type the command below.

``` Batch
git checkout master
```

I can now proceed with the merge operation with the **fix-invalid-email** branch.

``` Batch
git merge fix-invalid-email
```

After merging the branches I need to push all the modifications into the repository.
``` Batch
git push
```
Now i would like to tag this last push, and for it I created a new tag and pushed it into the origin.
As it is possible to acknowledge in the image below:

  ![](.\images\SecondMerge.png)
  
``` Batch
git tag -a v1.3.3 -m "Merging the branch fix-invalid-email with master

git push origin v1.3.3
```
In my last commit, firstly i added all the files that had some modifications. 
``` Batch
git add .
```
Commited all the files with a simple message.
``` Batch
git commit -am "End of the assignment"
```
Pushed this last commit.
``` Batch
git push
```
Created another tag, with a message.
``` Batch
git tag -a ca1 -m "End of the assignment"
```
And finally pushed into the origin, has we can prove it by the next image:

![](.\images\LastCommit.png)
``` Batch
git push origin ca1
```
---
### Report working with GIT with the help of a script named ManagingGit.bat

**ManagingGit.bat** is a script that was developed by group 3, which we decided that would be a great starting challenge.

I decided to report this script developed in another section in order not to 
cause any confusion when presenting the solution using just the command line.
Therefore, i will analyze every aspect of the script demonstrating it capacities and functionalities.

In the image below, it is possible to see that the first command that will run automatically when we run the ManagingGit.bat, is git status.

With this we pretend to present to the user the status of the repository (files to add/ files not staged).

![](.\images\ManagingGitMainMenu.png)

Pressing **1** and after it pressing ENTER, we'll have access to the command `git diff --staged`, in order to see
 all the differences regarding the files that we can add in the next commit (image below). After pressing any key, 
 the command `git status` will run automatically, will direct the user to the main menu.

![](.\images\ManagingGitDiff.png)

Pressing the key **2** and then press ENTER, the command `git add` will run, asking us the file that we'd like to add to the next commit.

![](.\images\ManagingGitGitAdd.png)

The number 3 of the main menu, is to add all the files, so in other words it'll run the command `git add .`

In the number 4 we have access to commit all files added previously, the command used it this part of the script is `commit -am [message]`. 

![](.\images\ManagingGitCommitAll.png)

The number 5 of the main menu is the command `git push`.

On the number 6 of the main menu we have access to the command `git pull`.

The number 7 of the main menu, is a submenu  enabling the user to manage all the files in terms of unstaging or removing them or even discard the changes.
To do so, and having in consideration the option selected, the script will run automatically the command `git restore --staged <FILETORESTORE>`
(if U is typed), or `git rm --cached >REMOVECACHEDFILE>` (if RC is typed) or  `git restore <FILETORESTORE>` (if DC is typed). 
Please be advised that the important thing is the letters typed, not if they are in lowercase or uppercase. 

We added only the image below due to the fact that we consider that is capable of represent all the 3 options, which 
have the same steps (after selecting the option the script will ask the name of the file).

![](.\images\ManagingGitRestoreUnstage.png)

In order to check all the commit history we need to press 8, and the script will run automatically the command `git log`.

Choosing to press the number 9, the user will have access to the command `git branch` and more other options, as it is possible
 to acknowledge in the image below.
 
![](.\images\ManagingGitBranch.png)

If the user want to create a new branch, he should choose to press the number 1, in order to run the command `git branch %NEWBRANCH%`.

Please notice that after choosing to create a new branch, the script will request more information regarding the name of the branch that
the user want to create, as it can be seen in the image below. This prompt will be assigned to the variable named `NEWBRANCH`.

By this, the script will have the full command needed to create a new branch.

![](.\images\ManagingGitBranchCreate.png)

If the user want to change to another branch, he should choose to press the number 2, in order to run the command `git checkout %BRANCHTOCHANGE%`.

Please notice that after choosing to change to another branch, the script will request more information regarding the name of the branch that
the user want to change, as it can be seen in the image below. This prompt will be assigned to the variable named `BRANCHTOCHANGE`.

With this, the script will have the full command needed to change to another branch.

![](.\images\ManagingGitBranchChange.png)

If the user want to merge branches, he should choose to press the number 3, in order to run the command `git merge <%ERGEBRANCHES%`.

Please notice that after choosing to merge branches, the script will request more information regarding the name of the branch that
the user want to merge with, as it can be seen in the image below. This prompt will be assigned to the variable named `MERGEBRANCHES`.

By this, the script will have the full command needed to merge the master branch with another one selected by the user.

![](.\images\ManagingGitBranchMerge.png)

If the user want to delete a new branch, he should choose to press the number 4, in order to run the command `git branch -d %BRANCHTODELETE%`.

Please notice that after choosing to delete a branch, the script will request more information regarding the name of the branch that
the user want to delete, as it can be seen in the image below. This prompt will be assigned to the variable named `BRANCHTODELETE`.

With this, the script will have the full command needed to create a new branch.

![](.\images\ManagingGitBranchDelete.png)

If the user want to go to the previous menu (main menu), he can press 5 and the script will run automatically the command `@goto start`.
With this the user will be sent to the beginning of the script where we defined the start of the it using `:start`.

If the user want to exit the script,  he can press 6 and the script will run automatically the command `exit /b`.

In case the user type "t", he will have access to write a command like if he was on the command line and not on a script.
In order to the script to run the command typed the script will have only to call the command given by the user.

In other words the command typed will be assigned to a variable called `COMMAND` and after it will be called by this command `call %COMMAND%`.

![](.\images\ManagingGitTypeCommand.png)

In case the user type "tag", he will have access to a simple submenu where he can create tags (pressing 1) or removing tags (pressing 2). 
And not less important if the user press 3 he can go to the previous menu.

If the user want to create a new tag,  he can press 1 and the script will run automatically asking 2 simple questions. 
The name of the tag (being assigned to the variable TAG), and a message that the user might want to add to the tag that he's about to create 
(which will be assigned to the variable `TAGMESSAGE`).

With this, the script will have the full command needed to create a new tag, so the script will run the command `git tag -a %TAG% -m %TAGMESSAGE%`.

Once the tag is created the script will run automatically the command `git push origin %TAG%`.

![](.\images\ManagingGitBranchTAG.png)

If the user want to delete a tag,  he can press 2 and the script will run automatically ask the name of the tag that the user want to delete. 
The name of the tag given by the user will be assigned to the variable `TAGTOREMOVE`.

With this, the script will have the full command needed to delete a tag, so the script will run the command `git tag -d %TAGTOREMOVE%`.

![](.\images\ManagingGitBranchTAGRemove.png)

In case the user type "mtag", the script will run automatically through some steps in order to merge two branches, pushing the changes, and 
add a tag to this push.

Please notice that after choosing this option, the script will request more information regarding the name of the branch that
the user want to merge with, as it can be seen in the image below. Have in consideration that this prompt will be assigned to the variable 
named `%MERGEBRANCHES%`.

With this, the script will have the full command needed to merge two branches, so the script will run the command `git merge %MERGEBRANCHES%`.

Once the merge is completed the script will run automatically the command `git push`, as it is possible to acknowledge in the image below.

The next step of the script is to request to the user the name of the tag (assigned to the variable `TAG`) that he's about to create and some 
description for it (assigned to the variable `TAGMESSAGE`), like the example in the image below.

Receiving that information the script has all the information to create the tag refereed by the user by the command `git tag -a %TAG% -m %TAGMESSAGE%`.
After creating the tag the script will push the tag to the origin using the command `git push origin %TAG%`.

![](.\images\ManagingGitBranchMergeTAG.png)

![](.\images\ManagingGitBranchMergeTAG_Name.png)

In case the user type "ctag", the script will run automatically through some steps in order to commit all added files, and 
add a tag to this push.

Please notice that after choosing this option, the script will request more information regarding the description of the commit that
the user want to push, as it can be seen in the image below. Have in consideration that this prompt will be assigned to the variable 
named `%TAGMESSAGE%`.

With this, the script will have the full command needed to commit the files added, so the script will run the command `git commit -am <MESSAGE>`.

Once the commit is completed the script will run automatically the command `git push`, as it is possible to acknowledge in the image below.

The next step of the script is to request to the user the name of the tag (assigned to the variable `TAG`) that he's about to create and some 
description for it (assigned to the variable `TAGMESSAGE`), like the example in the image below.

Receiving that information the script has all the information to create the tag referred by the user by the command `git tag -a %TAG% -m %TAGMESSAGE%`.
After creating the tag the script will push the tag to the origin using the command `git push origin %TAG%`.

![](.\images\ManagingGitCommitTAG.png)

![](.\images\ManagingGitBranchCommitTAG.png)


***


## Analysis of an Alternative / Mercurial



Git and Mercurial were created in the same year (2005), nevertheless, and despite the fact that Mercurial is used by a handful of 
large development organizations such like Facebook and Mozilla, it's growth hasn't been as strong and consolidated as the growth of Git. 

In order to get to know a little bit more of Mercurial, we can refer it has:

* Having simpler syntax when compared too git;
* Not allowing to change history.

Just another curiosity, compared to GIT, if we want to add branches, in Mercurial, we can do something similar named bookmarks.

--- 
### Report working with Mercurial with the command line

First of all I created a repository on [Perforce](https://www.perforce.com/products/helix-teamhub/free-account "PERFORCE Homepage"),
 and after I've cloned it through the command line in the folder that I wanted to save my work.

In order to clone the repository created I typed in the command line:

``` Batch
hg clone https://1121016isepipppt@helixteamhub.cloud/shiny-suitcase-6904/projects/teste2/repositories/mercurial/devops-19-20-A-1121016
```

Despite the fact that in the repository that i've shared has some errors, now i'll proceed correctly. And having 
this in mindfirstly i should create a bookmark. Which in this case I named this bookmark as "email-field". 
Once created I needed to push it to the repository.

``` Batch
hg bookmark email-field

hg push -B email-field
```
Next step was to change bookmarks in order i could be able to work on it (changed to email-field).

``` Batch
hg checkout email-field
```

After pasting the folder CA1 on the folder repository, I needed to add this changes to the hosted repository on Perforce.

In order to do so i needed add all the files.

``` Batch
hg add
```

After it I had to commit all the changes referring the issue previously created on Perforce, and add a simple message.

![](.\images\MercurialRef1.png)

``` Batch
hg commit -m "ref #1 - Initial version"
```
Now i could push all the changes made in my repository.

``` Batch
hg push
```
In order to add a tag to the previous push i created a tag and after it i pushed to the repository, 
as it is possible to acknowledge in the image below.

![](.\images\MercurialTAGV1.2.0.png)

``` Batch
hg tag v1.2.0

hg push
```

In order to add the email-field in the application I decided to create a bookmark. To do so, firstly I needed to check 
the list of all the bookmarks (just to be sure that it was empty).

``` Batch
hg bookmark
```

And after it i created a bookmark which I named "email-field" which i needed to push it to the repository.
``` Batch
hg bookmark email-field
hg push -B email-field
```

Next step was to change bookmarks in order i could be able to work on it (changed to email-field).
``` Batch
hg checkout email-field
```

After making all the changes desired I wanted to make my first push of the bookmark email-field, to do so 
I needed to commit all the changes and only after it i could push it into the repository. Giving us the commit below:

![](.\images\MercurialTAGV1.3.0.png)
``` Batch
hg commit -m ref #2 - Email feature added

hg push
```

Now i would like to tag this last push, and for it I created a new tag and pushed it into the repository.

``` Batch
hg tag v1.3.0

hg push
```

And after it i created a bookmark which I named "fix-invalid-email" which i needed to push it to the repository.
``` Batch
hg bookmark fix-invalid-email
hg push -B fix-invalid-email
```
Next step was to change bookmarks in order i could be able to work on it (changed from **email-field** to **fix-invalid-email**).
``` Batch
hg checkout fix-invalid-email
```

After making all the changes desired I wanted to make my first push of the bookmark **fix-invalid-email**, to do so 
I needed to commit all the changes and only after it i could push it into the repository. Giving us the commit below:

![](.\images\MercurialFixInvalidEmail.png)

``` Batch
hg add

hg commit -m "ref #3 - Fix the bug on the email feature"

hg push

```
After committing all the changes into the fix-invalid-email bookmark, I needed to merge this bookmark with the email-field bookmark.

To do so I can proceed with the merge operation with the email-field bookmark.

``` Batch
hg merge email-field
```

After merging the bookmarks I need to push all the modifications into the repository.
So firstly i committed all the changes and created a new tag. And only after it i pushed to the repository, as it can 
be seen through the image below.

Unfortunately i made a mistake and wrote "**Branches merged**" instead of writing "**Bookmarks merged**".

![](.\images\MercurialBookmarksMerged.png)
``` Batch
hg commit -m "ref #3 - Bookmarks merged"

hg tag v1.3.1

hg push

```
In order to have my working directory up to date with the repository merged and updated i needed to type the following command:

``` Batch
hg update
```

Finally, i would like to tag my last push, and for it I created a new tag and pushed it into the email-field bookmark, has it can 
be seen in the image below:

![](.\images\MercurialLastCommit.png)

To do so firstly i added all the files that had some modifications.

``` Batch
hg add
``` 
Committed all the files with a simple message.

``` Batch
hg commit -m "End of assignment"
``` 

Created another tag.

``` Batch
hg tag ca1
``` 

And finally pushed into the origin, has we can prove it by the next image:

``` Batch
hg push
```
