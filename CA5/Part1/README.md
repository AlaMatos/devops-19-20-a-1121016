## Class Assignment 5/Part1

## What' is Jenkins

####Theoretical contextualization

First of all we need to analyse the main differences between CI (Continuous integration) and CL (Continuous delivery).

By this, Continuous Integration is named as a best practice in terms of software development in which is required to 
the developers to commit their changes frequently, to the source code in a shared version control repository. 

With this so called mindset, the company will be engaging the visibility, enabling all the engineers on a project 
to have all the information regarding the changes in the base code. 

In order to have a more automated tool, it is possible to configure the server, so it sends an alert to the developers 
when their commit failed in some point of the code, so that they can fix any errors as soon as possible.
This automation allows the teams to automate testing, like for instance, unit tests or integration tests, to detect 
the problems early in the development process. 

![](./Images/CONTINUOUS_INTEGRATION.PNG)

In terms of Continuous delivery, besides building the application and running tests on it, the application is also sent to
 a server where someone will perform manual tests on it.
This will make product deployments smaller which will reduce deployment risk. 

In other words, regular and smaller deliveries are less risky than bigger deliveries that would occur once or twice a year.

![](./Images/CONTINUOUS_DELIVERY.PNG)


___

###Jenkins
Regarding Jenkins, which is an open source automation tool written in Java with plugins built for Continuous Integration purpose.
It is used to continuously build and test our software projects making it easier to integrate changes to each
 project.


Jenkins achieves Continuous Integration with the help of plugins, which allows the integration of various DevOps 
stages (as it can be acknowledged by the image below). If we want to integrate a particular tool, we only need to install
 the plugins for that tool. For example: Git, Maven, Gradle, Docker, and so on.

![](./Images/JENKINS_STAGES.PNG)

In just a few words, we can acknowledge that Jenkins offers a simple way to set up a CI/CD environment for almost 
any combination of programming languages and source code repositories using pipelines.

_________________________________________________________________________________________________


### Report working with Jenkins


Firstly i needed to download the ``jenkins.war`` file from the [Jenkins website](https://www.jenkins.io/doc/book/installing/#war-file) 
in order to be able to work with Jenkins on my localhost.

On the directory that i have the downloaded file, and in order to star **Jenkins** I need to run the following command:

```Bash
java -jar jenkins.war
```

After it i needed to define the port for the Jenkins URL, as it is shown by the image below:

![](./Images/JENKINS.PNG)

In order to unlock jenkins i needed to enter the password that was created in the file ``initialAdminPassword``, as follows:

![](./Images/INITIAL.PNG)

After unlocking Jenkins and installing the usual plugins, i created a new Job, as it can be seen by the image below:

![](./Images/CREATE_NEW_JOB.PNG)

The next step is to create a **Jenkinsfile** in order to add it to a pipeline script from SCM ( Source Control Management).

Although the Jenkins file is illustrated by the next image, i'll go through some points of the file, in a few moments, 
in order to have a better understanding of it.


![](./Images/JENKINS_FILE.PNG)

So, after creating a Jenkins file i need to push it to my repository and only after it i can define a new **Pipeline 
script from SCM**.

Having that in mind, in order to define the **pipeline script from SCM** i need to define the URL of my repository,
as well as the location of the Jenkins file that i pushed to my repository.

All these configurations can be analyzed within the next image:

![](./Images/PIPELINE_SCM.PNG)

Regarding the analysis of the Jenkins File:

 + I used the URL of my repository in order to provide the code to execute the  next stages of my pipeline.
 
 ```Bash
stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://AlaMatos@bitbucket.org/AlaMatos/devops-19-20-a-1121016.git'
            }
        }
```
 + On this stage i want to build my application using ``gradle`` but without testing my app.  To do so, i used the command 
 ``-x``. Which in gradle refers to "exclude" a determined task, that in this case is the tests. I could also use the command 
 ``--exclude-task``, to exclude the tests from running.
 
 I also used the ``dir`` option in order to refer to the directory where i have the ``gradlew`` file.
  
  

```Bash
stage('Assemble') {
            steps {
               dir("CA2/Part1"){
                   echo 'Assembling...'
                   sh './gradlew clean build -x test'
               }
             }
        }
```
 + On this stage i want to test my application using ``gradle``.
 
```Bash
stage('Test') {
             steps {
                dir("CA2/Part1"){
                    echo 'Testing...'
                    sh './gradlew test'
                 }
              }
         }
```

+ On this stage i want to store all the files within the ``build/distributions/`` on my Jenkins, associated to each 
successful build of my application.

```Bash
 stage('Archive') {
             steps {
                 dir("CA2/Part1"){
                 echo 'Archiving...'
                 archiveArtifacts 'build/distributions/*'
                 }
             }
         }
```

As it can be shown by the next images:
 
 + In the **Build artifacts** section on the **status** page of each successful build:

![](./Images/BUILD_STATUS.PNG)

 + Within the folder ``distributions`` on my **Workspace**:
 
![](./Images/WORKSPACE.PNG)

 + On this stage i want to define aditional steps, which in this case is to add to an ``xml`` file the result of the 
 tests that ran previously.
 
  The ``always`` condition block means that what's within curly braces should run regardless the output of the pipeline.
 
```Bash
post {
      always {
        junit '**/test-results/test/*.xml'
      }
   }
```
The ``xml`` file created can be shown by the next image:

![](./Images/POST.PNG)

After successfully build the pipeline created, i had the following result in the **Stage view**:

![](./Images/STAGE_VIEW.PNG)

Regarding the output of the test, we can acknowledged by the following image that the test ahas passed:

![](./Images/OUTPUT_OF_TESTS.PNG)

FInally i had the following summary of the job created relative to the building status (name of the job, last success, last 
unsuccess and duration of the last building).

![](./Images/JOB.PNG)

___
#Jenkins

The CA5/part2 assignment comparatively to the CA5/Part1 had a few changes to be added. However, i'll be analyzing 
all the stages that was needed to finish this class assignment.

Similarly to the previous assignment (CA5/Part1) i need to create a **Jenkinsfile** in order to add it to a pipeline 
script from SCM (Source Control Management). But first, i needed to push it to my repository and only after it i can 
define a new **Pipeline script from SCM**.

In order to pass all the needed information step by step, i'll go through all stages of the file,
in order to have a better understanding of it.

Having that in mind, in order to define the **pipeline script from SCM** i need to define the URL of my repository,
as well as the location of the Jenkins file that i pushed to my repository.

All these configurations can be analyzed within the next image:

![](./Images/PIPELINE_SCM_PART2.PNG)


Regarding the analysis of the Jenkins File:

 + I used the URL of my repository in order to provide the code to execute the  next stages of my pipeline.
 
 ```Bash
stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://AlaMatos@bitbucket.org/AlaMatos/devops-19-20-a-1121016.git'
            }
        }
```

 + On this stage i want to build my application using ``gradle`` but without testing my app, similarly to the previous 
 assignment.
To do so, i used the command ``-x``. Which in gradle refers to "exclude" a determined task, that in this case is the 
tests. I could also use the command ``--exclude-task``, to exclude the tests from running.
 
 I also used the ``dir`` option in order to refer to the directory where i have the ``gradlew`` file.  

```Bash
stage('Assemble') {
            steps {
               dir("CA2/Part2"){
                   echo 'Assembling...'
                   sh './gradlew clean build -x test'
               }
             }
        }
```
 + On this stage i want to test my application using ``gradle``.
 
```Bash
stage('Test') {
             steps {
                dir("CA2/Part2"){
                    echo 'Testing...'
                    sh './gradlew test'
                 }
              }
         }
```
 + On this stage i want to generate the **Javadoc** files and publish them. In order to generate the Javadoc I need to use
  ``./gradlew javadoc`` command, and the plugin HTML publisher, where all the information regarding this plugin can be 
  found on this [Jenkins documentation page](https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/).
  
  Regarding the configuration parameters, I used the following:
  
  + allowMissing (false) - won't allow reports that are missing;
  + keepAll (true) - will archive reports for all successful builds;
  + alwaysLinkToLastBuild (true) - will publish the link on project level even if build failed.
 
```Bash
stage('Javadoc') {
          steps {
             dir("CA2/Part2"){
                 echo 'Generating the Javadoc documentation...'
                 sh './gradlew javadoc'
                 publishHTML (target : [allowMissing: false,
                 alwaysLinkToLastBuild: true,
                 keepAll: true,
                 reportDir: 'build/docs/javadoc/com/greglturnquist/payroll/',
                 reportFiles: 'myreport.html',
                 reportName: 'My Reports',
                 reportTitles: 'The Report'])
              }
           }
      }
```
By this the Javadoc files can be acknowledge in the following images:

+   On Jenkins workspace:

![](./Images/JENKINS_JAVADOC.PNG)

+   On the browser:

![](./Images/JENKINS_JAVADOC_PAGE.PNG)

+ On this stage I want to store all the files within the ``build/libs/`` on my Jenkins, associated to each 
successful build of my application.

```Bash
 stage('Archive') {
             steps {
                 dir("CA2/Part2"){
                 echo 'Archiving...'
                 archiveArtifacts 'build/libs/*'
                 }
             }
         }
```

As it can be shown by the next images:
 
 + In the **Build artifacts** section on the **status** page of each successful build:

![](./Images/JENKINS_BUILD_STATUS.PNG)

 + Within the folder ``libs`` on my **Workspace**:
 
![](./Images/WORKSPACE_PART2.PNG)

+ On this stage I want to create a docker image of each successful Jenkins build, and push it to my docker hub.

In order to be able to create a docker image I need to have a Dockerfile in the same directory that i have the Jenkinsfile,
as it can be acknowledge by the image below:

![](./Images/JENKINS_DOCKERFILE_JENKINSFILE.PNG)

The docker file has the command to copy file ``demo-0.0.1-SNAPSHOT.war`` into the **tomcat8 server**, as it can be seen 
by the image below:

![](./Images/DOCKER_FILE.PNG)

Next step to push a docker image to Docker hub, is to add my docker hub credentials into Jenkins, as it can be 
acknowledged by the image below:

![](./Images/DOCKERHUB_CREDENTIALS.PNG)

The following code it is meant to build a docker image with a tag of the **ID** of Jenkins last successful build, and push it
to my docker hub.

```Bash
 stage ('Docker Image') {
             steps {
                 dir("CA2/Part2"){
                 script {
                         def myLatestDockerImage = docker.build("1121016/devops-19-20-a-1121016:${env.BUILD_ID}")
                         docker.withRegistry('https://registry.hub.docker.com', 'DockerHubCredentials') {
                         myLatestDockerImage.push()
                     }
                 }
             }
         }
     }
```

After running my pipeline script ity appeared me this error:

![](./Images/CREDENTIALS_DOCKERHUB_ERROR.PNG)

Which was solved by approving the permission previously added to my Jenkins, as follows:

![](./Images/CREDENTIALS_DOCKERHUB.PNG)

![](./Images/CREDENTIALS_DOCKERHUB_2.PNG)

After the successfull Jenkins build i need to confirm if the docker image was pushed to my docker hub. Which can be 
acknowledged by the image below:

![](./Images/PUSHED_IMAGE_JENKINS.PNG)

I can also make a primary check in the docker hub if a file was actually copied by the image below: 

![](./Images/DOCKER_HUB_JENKINS.PNG)

However i'll check if the copied file was the correct one in a few moments.

 + On this stage i want to define additional steps, which in this case is to add to an ``xml`` file the result of the 
 tests that ran previously.
 
  The ``always`` condition block means that what's within curly braces should run regardless the output of the pipeline.
 
```Bash
post {
      always {
        echo 'This is the result of our tests!!'
        junit '**/test-results/test/*.xml'
      }
   }
```
The ``xml`` file created can be shown by the next image:

![](./Images/JENKINS_POST_PART2.PNG)

After successfully build the pipeline created, i had the following result in the **Stage view**:

![](./Images/STAGE_VIEW_LAST_BUILD_JENKINS.PNG)

Regarding the output of the test, we can acknowledged by the following image that all the tests has passed:

![](./Images/JENKINS_TESTS_PART2.PNG)

Finally i had the following summary of the job created relative to the building status (name of the job, last success, last 
unsuccess and duration of the last building).

![](./Images/JOBS_JENKINS.PNG)

In order to check if the docker image pushed to my docker hub repository, has the last build of the jenkins file, i must:

 + Pull the image previously pushed, using the following command:
    
```Bash
docker pull 1121016/devops-19-20-a-1121016:79
```
![](./Images/DOCKER_IMAGE_PULL_JENKINS.PNG)

+ Acknowledge the images that i have on my localhost, using the following command:

```Bash
docker images
```

![](./Images/DOCKER_IMAGE_LOCALHOST_JENKINS.PNG)

+ Run the container previously pulled using the **ImageID**, using the following command:

```Bash
docker run 1335f17230a5
```
+ Acknowledge the images that are running on my localhost, using the following command:
```Bash
docker ps
```
![](./Images/DOCKER_IMAGE_RUNNING_JENKINS.PNG)


+ Run the ``bash`` command on the container that I started previously, using the **ContainerID**, through the following command:

```Bash
docker exec -it 03f9957a6f26 bash
```
By the image below it is possible to acknowledge that the file copied to the docker image is in fact the ``demo-0.0.1-SNAPSHOT.WAR``.

![](./Images/ACESSING_DOCKER_IMAGE_JENKINS.PNG)

___

## Analysis of an Alternative / Bitbucket-Pipelines

**Bitbucket-Pipelines** is an integrated Continuous Integration and Continuous Delivery service, as much as **Jenkins**, however with
 a few differences which i'll analyse.

Regarding the **Bitbucket-pipelines**, it allow us to build automatically every commit that we make, or it can be run manually,
 in order to build, test and deploy our code. 
 
 In order to configure the pipeline in **Bitbucket**, it must be done through a yaml file.
 
One of the main difference is the flexibility. 

   + **Bitbucket-pipelines** is capable of doing some work, when we are talking about simple projects (like for example the CA5/Part2).
    It has some pipes (in Jenkins are called plugins), nevertheless are so few when compared to Jenkins. And in terms of documentation,
    also due to the fact that the number of users aren't as large as the number of Jenkins enthusiasts, is a little scarce. 
    
   + **Jenkins** on the other way as plenty of plugins, which are capable of suppress all our project needs, no matter what
   particular need we have (because if you want a particular functionality we can develop a new plugin). In terms of 
   documentation, due to the high number of users, as plenty of information through the web.

Long story short, Jenkins is a more flexible tool. There is really no limit to what we can accomplish with Jenkins Pipeline.

In terms of builds of the projects we have also some differences.

   + **Bitbucket-pipelines** builds always run a Docker image (at least). Nevertheless the user can use different Docker image 
    in each step of the project build, which means that each stage can run in a different container.
    
   + **Jenkins** uses a Master/Agent architecture. Basically the Jenkins Master coordinates builds across one or many Agents.
    Agents in a simple way are just a software that is installed on a machine (physical server, for example) that coordinates builds. 

In terms of builds either tools support docker images, however, **Bitbucket-pipelines** forces us to use it, but on the 
other hand **Jenkins** let us choose how we what to work.

In conclusion, **Bitbucket-pipelines** is a useful tool to begin in this CI/CD world, nevertheless if we need more functionalities
 we need to move to **Jenkins**.

___

###Implementation of the alternative using Bitbucket- Pipelines
In order to start working with **Bitbucket-pipelines** I must enable it in the repository settings, as follows:

![](./Images/ACTIVATE_BITBUCKET_PIPELINES.PNG)


Regarding the analysis of the **Bitbucket-pipelines.yml** file:

 +  This first 3 lines, are meant to refer that:
    + This code if a pipeline;
    + The ``custom`` designation is meant to refer that this pipeline will only run manually;
    + The ``master`` designation is used to refer that this pipeline will run only on **master** branch.
    
```Bash
pipelines:
  custom:
    master:
```
In order to run manually the pipeline i must select the branch as well as the pipeline, as it is demonstrated by the 
following image:
             
![](./Images/BITBUCKET_PIPELINES_RUN_PIPELINE.PNG)

 + On this next stage i want to build my application using ``gradle`` but without testing my app, similarly to the previous 
 assignment.
 
 In order to have the updated build on my repository i need to commit all the changes and push it to my repository, as it 
 can be acknowledge by the next image:
   
![](./Images/BITBUCKET_PIPELINES_ASSEMBLING.PNG)

```Bash
    - step:
          name: Assembling...
          script:
            - cd CA2/Part2
            - chmod +x gradlew
            - bash gradlew clean build -x test
            - git add -A #Stages all changes
            - git commit -m "war file build nr. $BITBUCKET_BUILD_NUMBER"
            - git push
```
 + On this stage i want to test my application using ``gradle``.
 
 After running the pipeline i could acknowledge that all the tests passed, as it can be seen by the image below:
 
![](./Images/BITBUCKET_PIPELINES_TESTS.PNG)

 In order to have the tests updated on my repository i need to commit all the changes and push it to my repository, as it 
  can be acknowledge by the next image:
  
 ![](./Images/BITBUCKET_PIPELINES_TEST_REPORTS.PNG)
 
```Bash
    - step:
          name: Test and archive...
          script:
            - cd CA2/Part2
            - git pull
            - chmod +x gradlew
            - bash gradlew test
            - git add -A #Stages all changes
            - git commit -m "test reports build nr. $BITBUCKET_BUILD_NUMBER"
            - git push
```

 + On this stage i want to generate the **Javadoc** files. In order to generate the Javadoc I need to use
  ``./gradlew javadoc`` command.
  
  In order to have the Javadocs updated on my repository i need to commit all the changes and push it to my repository, as it 
    can be acknowledge by the next image:
    
 ![](./Images/BITBUCKET_PIPELINES_JAVADOCS.PNG)
 
```Bash
    - step:
          name: Generating the Javadoc documentation...
          script:
            - cd CA2/Part2
            - git pull
            - chmod +x gradlew
            - bash gradlew javadoc
            - git add -A #Stages all changes
            - git commit -m "javadoc build nr. $BITBUCKET_BUILD_NUMBER"
            - git push
```

+ On this stage I want to store all the files within the ``build/libs/`` on my **Bitbucket**, associated to each 
successful build of my application.

On the next image it is possible to acknowledge that after building successfully it will store the artifacts for 14 days:
    
![](./Images/BITBUCKET_PIPELINE_ARTIFACTS.PNG)

```Bash
    - step:
           name: Archive...
           script:
             - cd CA2/Part2
             - git pull
             - git tag -a build_nr._$BITBUCKET_BUILD_NUMBER -m "Build nr. $BITBUCKET_BUILD_NUMBER"
             - git push origin build_nr._$BITBUCKET_BUILD_NUMBER
    
           artifacts:
             - CA2/Part2/build/libs/*.war
```

+ On this stage I want to create a docker image of each successful **Bitbucket pipelines** build, and push it to my docker hub.

In order to be able to create a docker image I chose to have a **Dockerfile** in the directory that i'm currently working on (CA2/Part2),
as it can be acknowledge by the image below:

![](./Images/JENKINS_DOCKERFILE_JENKINSFILE.PNG)

The docker file has the command to copy file ``demo-0.0.1-SNAPSHOT.war`` into the **tomcat8 server**, as it can be seen 
by the image below:

![](./Images/DOCKER_FILE.PNG)

Next step in order to push a docker image to Docker hub, I need to add my docker hub credentials into **Bitbucket pipelines**, as it can be 
acknowledged by the image below:

![](./Images/DOCKER_HUB_BITBUCKET_PIPELINES_VARIABLES.PNG)

The following code is meant to build a docker image with a tag of the **COMMIT** of **Bitbucket pipelines** last 
successful build, and push it to my docker hub. Which in this case the docker image should have the tag **9b1d47b**, 
referencing the last commit made by the user, as it demonstrated by the image below:

![](./Images/BITBUCKET_PIPELINE_LAST_COMMIT_MADE_BY_USER.PNG)

It is important to be aware that in order to enable access to Docker, i need to add docker as a service. And to do so, i 
need to use the following command:

```Bash
options:
  docker: true
```

In this code it is possible to acknowledge the structure of the pipeline step with the purpose of building and pushing
a docker image to docker hub.

```Bash
 - step:
           name: Building and pushing a docker image..
           script:
             - cd CA2/Part2
             - docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_PASSWORD
             - export BITBUCKET_COMMIT_SHORTER_NAME="${BITBUCKET_COMMIT::7}"
             - docker build -t 1121016/devops-19-20-a-1121016:$BITBUCKET_COMMIT_SHORTER_NAME .
             - docker push 1121016/devops-19-20-a-1121016:$BITBUCKET_COMMIT_SHORTER_NAME

options:
  docker: true
```

After receiving the information that the build was successful, as demonstrated by the image below:

![](./Images/BITBUCKET_PIPELINE_STAGE_VIEW.PNG)

 After **Bitbucket-pipelines** build i can verify the new commits done regarding the stages of **Assembling**,
 **Test and archive** and **Javadoc documentation**.

![](./Images/BITBUCKET_PIPELINE_COMMITS_DURING_BUILD.PNG)

I can also ckeck if the docker image was pushed to my docker hub. 

It can be also acknowledged by the image below, that the tag of this docker image pushed is the last commit made:

![](./Images/PUSHED_IMAGE_BITBUCKET_PIPELINES.PNG)

I can also make a primary check in the docker hub if a file was actually copied by the image below: 

![](./Images/DOCKER_HUB_BITBUCKET_PIPELINES.PNG)


In order to check if the docker image pushed to my docker hub repository, has the last build of the jenkins file, i must:

 + Pull the image previously pushed, using the following command:

```Bash
docker pull 1121016/devops-19-20-a-1121016:9b1d47b
```
![](./Images/DOCKER_IMAGE_PULL_BITBUCKET_PIPELINES.PNG)

+ Acknowledge the images that I have on my localhost, using the following command:

```Bash
docker images
```
![](./Images/DOCKER_IMAGE_LOCALHOST_BITBUCKET_PIPELINES.PNG)

+ Run the container previously pulled using the **ImageID**, using the following command:

```Bash
 docker run 63583964e297
```
+ Acknowledge the images that are running on my localhost, using the following command:
```Bash
docker ps
```
![](./Images/DOCKER_IMAGE_RUNNING_BITBUCKET_PIPELINES.PNG)

+ Run the ``bash`` command on the container that I started previously, using the **ContainerID**, through the following command:

```Bash
docker exec -it ae212e89a1c2 bash
```
By the image below it is possible to acknowledge that the file copied to the docker image is in fact the ``demo-0.0.1-SNAPSHOT.WAR``.

![](./Images/ACCESSING_DOCKER_IMAGE_BITBUCKET_PIPELINES.PNG)




