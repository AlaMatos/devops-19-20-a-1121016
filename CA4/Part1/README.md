## Class Assignment 4/Part1

## What's the difference between VM and Containers

####Theoretical contextualization

**Deployment where virtualization rules:** It allow us to run multiple VM on a HOST. By this, virtualization allows applications to be isolated
between VMs.
Each VM is a full machine running all the components, including its own operating system, on top of the virtualized 
hardware.

**Deployment where Container deployment rules:** Containers are similar to VMs, nevertheless they share the same Operating System (OS) among 
the applications. Therefore, containers are considered lightweight. 

![](./Images/DIFFERENT_ERAS.PNG)


Containers is a technology for packaging the (compiled) code for an application along with the 
dependencies it needs. 

This technology became popular, due to the fact that presented the users with solutions more 
lightweight, by this meaning, that requires less memory to run, fast and agile when compared to VM’s.

This happens mainly due to the fact that containers virtualize only the OS (making possible to run multiple containers 
on a single OS), instead of virtualizing the hardware as the 
VM’s do. With this capability containers take seconds to start, instead of minutes that are necessary to VM’s to start.

Changing the focus to the virtualization aspect, it’s important to acknowledge that the VM’s have limited performance when 
compared to containers (mainly due to the need of hardware virtualization), and because containers take advantage of 
the host hardware, providing to the user a better performance.

The main disadvantages of containers is not allowing to run differents OS in a single host (contrary to VM’s), as well 
as regarding security issues, which might be easier to affect all the containers of a host.

![](./Images/CONTAINERS.PNG)


Intermsof the virtual machines, it could be a better choice when we are talking about running applications that require a lot 
of the operating system’s resources, or have a wide variety of operating systems to manage.

Regarding the containers, this technology is a better choice when our biggest priority is to maximize the number of 
applications running on a minimal number of servers.


### Report working with Docker Toolbox

Firstly i needed to download the Docker Toolbox from this [repository](https://github.com/docker/toolbox/releases) 
in order to be able to work on my localhost.

In order to check if Docker was installed on my machine i typed the command ``docker -v``, having the following outcome:

![](./Images/DOCKER_VERSION.PNG)

Due to the fact that on my previous assignment i used Hyper-V, in this assignment in order to be able to use Virtual box,
i need to firstly deactivate the Hyper-V and run the ``EnableVirtualBox.bat`` file as administrator and restart my pc.

For this kind of report i'll be only referring the source of that .bat file (check the link below), and add the referred 
file to the repository.

+ https://forums.virtualbox.org/viewtopic.php?t=41258#

In order to deactivate the Hyper-v on the localhost i had to unselect the Hyper-V option in the control panel, 
as it can be acknowledged by the image below:

![](./Images/HYPER-V.PNG)

Due to the fact that i'm using Docker Toolbox i need to run the Docker Quickstart Terminal, on my machine, every time 
i want to work with docker. The main reason for that is to set an IP address for my machine, which in this case is the 
IP address of the VM that docker is running. Meaning by this, when i want to check the outcome of my docker images
i need to substitute on the url the localhost for the IP address given to me by the Docker Quickstart Terminal.

In order to build the images with the correct information I needed to make some changes on the WEB_Dockerfile, as follows:

 + Update the repository;
 + change the ``WORKDIR`` where my application will be located;
 + Change the name of the war file to demo.
 
![](./Images/WEB_DOCKERFILE.PNG)

Regarding the file ``docker-compose.yml`` i needed to change the path of the volumes (directory of my localhost),
 as well as the path of the shared folder in the VM, as it is demonstrated by the images below:

![](./Images/DOCKER_COMPOSE_VOLUMES.PNG)

![](./Images/VM_CONFIG.PNG)

In order to build the images (DB and WEB) i used the following command:

```Bash
docker-compose build
```
In order to start the containers in the background putting them up and running, i typed the following command:

```Bash
docker-compose up
```

To check if both containers are up and running, i need to check it through the following IP addresses:

 + web - http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/

Presenting me with the following webpage:

![](./Images/WEB.PNG)

 + db -  http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console/
       
Presenting me with the following webpage where i have to add the JDBC URL, as it can be acknowledged by the following image:

![](./Images/DB_1.PNG)

After a successful connection, I run a command ``SELECT * FROM EMPLOYEE`` in order to prove that the DB has records,
as it can be seen by the image below:
      
![](./Images/DB_2.PNG)

In order to have a DB with more data, i decided to add a new "**Employee**", as it can be acknowledged by the images below:

![](./Images/DB_ADD_FIELD.PNG)

![](./Images/DB_ADD_FIELD_3.PNG)

![](./Images/WEB_ADDED_FIELD.PNG)

After the modifications on the DB, i needed to connect to the container of the DB in order to copy the DB file 
(jpadb.mv.db) to my local host.

To do so i needed to type the following commands:

 * To connect to the DB container:
 
```Bash
docker-compose exec db /bin/bash
```
 * To copy the jpadb.mv.db from the folder /usr/src/app to the folder /usr/src/data:
 
 ```Bash
 cp ./jpadb.mv.db /usr/src/data
 ```
In the image below it 's possible to observe all the steps token.

![](./Images/DB_ADDED_FIELD.PNG)

After making a backup of the DB, I stopped and removed the containers (by typping ``docker-compose down``) in order to test if
i was really capable of making a backup of the DB.

After typing the command ``docker-compose up``, i needed to:

 + Enter in the WEB container, as follows:
 
```Bash
docker-compose exec web /bin/bash
```
 + Change the IP address of the conection string H2 (using nano in order to access the application.properties file), 
 adding the folder /usr/src/data, where i have my backup, as it can  be acknowledged by the image below:
 
![](./Images/H2_STRING.PNG)

+ After it i must build the whole project, and to do so i must be in the directory of the gradle project that i want to build, 
and only after it i can type the following command to build the project:

```Batch
./gradlew clean build
```

+ The next needed step is to copy the new build file ``demo-0.0.1-SNAPSHOT.war`` into the tomcat8 server. 
To do so i needed to type the following command:

```Batch
cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps 
```

After all these steps i checked if everything was running without any problems, and to do so 
i needed to check the output of the WEB and DB containers, accessing the URL, as follows:


 + web - http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/

Presenting me with the following webpage:

![](./Images/WEB_ADDED_FIELD.PNG)

 + db -  http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console/
       
Presenting me with the following webpage where i have to add the JDBC URL where i have the DB backup file (/usr/src/data), 
as it can be acknowledged by the following image:

![](./Images/DB_JDBC.PNG)

Where i can recheck all the employees that my DB has. Which it can be acknowledged that it maintains the 3 employees 
that it had before i've stopped and removed the containers.
 
![](./Images/DB_ADD_FIELD_3.PNG)


The next task of the current assignment was to push the images created of the containers created.

Firstly i needed to create on [Docker hub](https://hub.docker.com/) an account and afterwards a new repository.

Now i'll need to login to my docker hub in order to be able to push the DB and WEB images that i previously created. 
And to do so i needed to type the following command:

```Bash
docker login docker.io
```

Printing on the command-line the following result:

![](./Images/DOCKER_HUB.PNG)

After it, i needed to type the following commands:

 * Check all the containers:
 
```Bash
docker ps -a
```

 * Commit the DB image to the docker hub repository previously created:
 
```Bash
docker commit 68c4ed5ab411 1121016/devops-19-20-a-1121016:DB
```

 * After it i needed to push it to the repository:

```Bash
docker push 1121016/devops-19-20-a-1121016
```
![](./Images/DB_IMAGE_PUSH.PNG)

 * Commit the WEB image to the docker hub repository previously created:
 
```Bash
docker commit 9b702fea419a 1121016/devops-19-20-a-1121016:WEB
``` 
 * After it i needed to push it to the repository:
 
```Bash
docker push 1121016/devops-19-20-a-1121016
```

![](./Images/WEB_IMAGE_PUSH.PNG)

The following image allows us to acknowledge that both images are on the same repository.
 
![](./Images/DB_WEB_IMAGES_PUSHED.PNG)



##Kubernetes


Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services.
Moreover, Kubernetes is a precedessor of Borg which has been developed by Google, who open-sourced the Kubernetes 
project in 2014, after being developed for over 15 years at Google.

To work with Kubernetes, we need to use Kubernetes API objects to describe our cluster’s desired state, by this i mean, 
the applications we want to run, what container images kubernetes should use, the number of replicas.

###Why we need Kubernetes
Containers are a good way to bundle and run our applications. In a production environment, we need to manage the 
containers that run the applications, preventing some kind of downtime. For example, if a container goes down, 
another container needs to start. 

Having that in mind, Kubernetes was developed in order to provide us the possibility to run distributed systems, taking 
care of scaling and downtime of our application. 

Relative to Kubernetes we have some components that we need to have some knowledge in order to understand what goes
under the hood.

Having this in mind, Kubernetes use the following components (which by the way are only a few of them):

+ PODS:

A Pod encapsulates an application’s container (or, in some cases, multiple containers), storage resources, a unique 
network identity (IP address), as well as options that govern how the container(s) should run.

By other words, a Pod represents processes running on our cluster (that we're going to analyze in a few moments).

![](./Images/PODS.PNG)

 + NODES:

A Pod always runs on a Node. A Node is a worker machine in Kubernetes which may be either a virtual 
or a physical machine.

Each Node is managed by the Master. A Node can have multiple pods, and the Kubernetes master automatically handles 
scheduling the pods across the Nodes in the cluster. 
 
 ![](./Images/NODE.PNG) 
 
 + Cluster:
 
 In simple words a Cluster is an highly available group (aka cluster) of computers that are connected to each other in order
 to work as a single unit. 
 
 By this, a Kubernetes cluster consists of two types of resources:
 
 + The Master which coordinates the cluster, basically the Master is responsible to manage the whole group of worker 
 machines;
 
 +	The Nodes are the workers that run applications.

 
 ![](./Images/CLUSTER.PNG) 
 
 
___
### Implementation of Kubernetes

In order to implement Kubernetes on this assingment, firstly i needed to set up my working environment(on my working 
directory), and for it i needed to:

 + Install [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/);
    + Is a tool that runs single-node (worker machine) in a VM on a localhost.
    
 + Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/);
    + A command-line tool that allow us by the means of commands to manage clusters.
     
 + Install [Kompose](https://kompose.io/installation/).
    + Is a tool that helps the users by translating the docker-compose file into Kubernetes resources.
    
The next step was to start minikube, where it'll be starting a new VM using Virtual Box. And to do so i typed the 
following command:

```Bash
minikube start
```

Giving me the following outcome:

![](./Images/MINIKUBE.PNG)  

After it in order to ckeck the status of the cluster i typed the command ``minikube status``, returning me the result below:

![](./Images/MINIKUBE_STATUS.PNG)

In order to check the if our node is ready, i can type the following command:

```Bash
kubectl get nodes
```
Giving me the confirmaton that minikube is up and ready, as it can be acknowledged by the image below:

![](./Images/KUBECTL_GET_NODES.PNG)

Now i'll need to login to my docker hub in order to be able to pull the DB and WEB images that i previously pushed. 
And to do so i needed to type the following command:

```Bash
docker login docker.io
```
Printing on the command-line following result:

![](./Images/DOCKER_HUB.PNG)
  
After it i needed to convert the docker-compose file into resources that Kubernetes would understand.
For it i needed to type the following command:

```Bash
kompose convert
```
Providing me the following outcome:

![](./Images/KOMPOSE_CONVERT.PNG)  

The next step is to create a resource from the files created by the ``Kompose convert``.

By this, in order to create 2 services (DB and WEB) and 2 deployments (DB and WEB), i needed to type the following 
command:
```Bash 
kubectl create -f db-service.yaml,web-service.yaml,db-deployment.yaml,web-deployment.yaml
```
Providing me the result that all the resources were created successfully, as it can be acknowledged by the image below:

![](./Images/KUBECTL_CREATE.PNG)

In order to check all the pods i typed the following command:
  
```Bash
kubectl get pods
```
Providing me the following information:

![](./Images/KUBECTL_GET_PODS.PNG)

In order to check all the deployments i typed the following command:

```Bash
kubectl get deployments
```
Giving me the following information:

![](./Images/KUBECTL_DEPLOYMENTS.PNG)

In order to check all the services i typed the following command:

```Bash
kubectl get services
```
And the outcome was the rpresented by the image below:

![](./Images/KUBECTL_SERVICES.PNG)

If i wanted to gather all this information in only one command, i must type:

```Bash
kubectl get all
```
And it will give me the following information:

![](./Images/KUBECTL_GET_ALL.PNG)

I have also access to this information and more in the browser, through Kubernetes dashboard. To do so i need to type 
the following command:

```Bash
minikube dashboard
```
Providing me, first the information below:

![](Images/MINIKUBE_DASHBOARD.PNG)

And after it openning a new browser window with all the information of my cluster, as it can be acknowledged by the image below:

![](Images/DASHBOARD_BROWSER.PNG)

In order to create a service to expose the deployment ``web``, i must type the following command:

```Bash
kubectl expose deployment web --type=NodePort --name=my-service-web
```
Providing me the confirmation that it was created as it can be acknowledge by the image below:

![](Images/KUBECTL_EXPOSE_WEB.PNG)

In order to access to a service using minikube, i needed to provide the service that i 'd like to access, and to
do so i needed to type the following command:
 
```Bash
minikube service my-service-web
```
Providing me the following information:

![](Images/MINIKUBE_SERVICE.PNG)

And redirecting me to the browser, nevertheless presenting me this message of error:

![](Images/ERROR_404.PNG)

Regarding the fact that i wasn't able to access my containers, i need to update the IP address on the application.properties
 file that i have in the WEB machine.
To do so, firstly i needed to access to the WEB machine, by typing the following command:

```Bash
kubectl exec web -it -- /bin/bash
```

To update the IP address on the application.properties file i could change it manually but i used the sed expression, 
as it can be acknowledged by the command below:

```Bash
sed -i -e 's/192.168.33.11:9092/192.168.99.116:31580/g' src/main/resources/application.properties
```

After it i must build the whole project, and to do so i must be in the directory of the gradle project that i want to build, 
and only after it i can type the following command to build the project:

```Bash
./gradlew clean build
```

The next needed step is to copy the new build file ``demo-0.0.1-SNAPSHOT.war`` into the tomcat8 server. 
To do so i needed to type the following command:

```Bash
cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps 
```

After all these steps it is time to test if our containers are fully configured, but unfortunatelly the error persisted.

Although i wasn't capable of implementing totally Kubernetes, it gave me a brand new idea of how we can manage scalability,
and sustainability of our applications.


