## Class Assignment 1
### Report working with GRADLE

First of all I created a new folder (CA2/Part1) in my local disk, and after i've 
downloaded the project for this assigment from this [repository](https://bitbucket.org/luisnogueira/gradle_basic_demo/).

After it i moved all the files and directory to the folder created.
Which i opened in IntelliJ in order to start working on it.

### Instructions provided in the readme file

Firstly i needed to go through some steps in order to make the app able to run.

So, for it i needed to:

**1 -** Start by building the project using the command:

```Batch
gradlew build
```
  
**2 -** Start the server in order to be able to launch the chat:

```Batch
 java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

**3 -** Launch the client (with this i would get only one window). In order to have several chat windows i needed to type the 
following command the number of times of clients that i want:

```Batch
gradlew runClient
```
In order to solve the **question 2** that had as main objective create a new task in order to start the server, i needed to add
in the **build.gradle** file the following code:

``` Gradle
task runServer(type: JavaExec, dependsOn: classes) {
    group = "DevOps"
    description = "Launches the server"
    classpath = sourceSets.main.runtimeClasspath
    main = 'basic_demo.ChatServerApp'
    args '59001'
}
```

Adding this new task i could be able of typing the following command instead of the one typed on the step 2 of the instructions provided.

```Batch
gradle -b build.gradle runServer
```
Next feature desired was to make possible to make backup of some directory.
To do so I created a new task called Backup, that has a type of Copy, as follows:

```Gradle
task Backup(type: Copy) {
    from '/src'
    into '/Backup'
}
```
The next task consists in adding the feature to compress the directory **src**, and copy them to a zip file, in a new directory called **Backup**.
```Gradle
task zipCompress(type: Zip) {
    from '/src'
    destinationDirectory = file('/Backup')
    archiveFileName ='Backup.zip'
}
```