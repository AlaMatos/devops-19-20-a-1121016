## Class Assignment 2
### Report working with GRADLE

First of all I created a new folder (CA2/Part2) in my local disk, and after it I 
copied the project CA1 in order to work with it for this assigment, from my local directory.

Which I opened in IntelliJ in order to start working on it.

### Instructions provided in the pdf file

Firstly I needed to create a new branch with the name of ``tut-basic-gradle``.
For it i used the line of code presented below:

```Batch
git branch tut-basic-gradle
```
And in order to work on it i needed to checkout for the branch created, through the line of code that
follows: 
```Batch
git checkout tut-basic-gradle
```

In order to develop a new gradle project first I needed to start a new gradle spring project. And to do so i needed to 
 access the [Spring initializr](https://start.spring.io) in order to download a zip file with a new gradle spring project 
 with some dependencies as it can be acknowledge by the following image:
 
 ![](.\images\SpringInitializr.png)
 
 After it I extracted the zip file into the folder CA2/Part2, in order to enable me to start working with gradle.
 Having in mind that the src folder is empty I deleted it and copied the folder **src** as well as the **webpack.conﬁg.js**
 and **package.json**. Since i'll need to build the gradle project using the webpack tool, i also deleted the folder
 **src/main/resources/static/built/**.
 
 Next step was to check if the localhost:8080 is empty. And for it i needed to run the following command, in order to run the 
 server:
  
```Batch
gradlew bootrun
``` 
I acknowledge that the localhost:8080 was empty (by presenting me a blank page), only due to the fact that the project 
that i was running was missing a frontend plugin.

In order to the project start managing the frontend i needed to add some lines of code in the plugins block in **build.gradle**,
 as follows:

```Batch
id "org.siouan.frontend" version "1.4.1"
```

And the end result is demonstrated by the following image:

 ![](.\images\FrontEndPlugin.png)
 
After it I added also in the **build.gradle** this code, that configures the previous plugin:

```Batch
frontend {
    nodeVersion = "12.13.1"
    assembleScript = "run webpack"
}
```

The next step in terms of configuration was to add the following code in the file **package.json**.

```Batch
"scripts": { 
    "watch": "webpack --watch -d", 
    "webpack": "webpack"
},
```

Now in order to generate the frontend code and all the tasks of it, i typed the following command in the command line:

```Batch
gradlew build
```

And only after it i can execute the spring application by typing:

```Batch
gradlew bootRun
```
Now we need to implement two features, as follows:

**1 -** Copy the generated jar to a folder named "dist" located at the project root folder level.
To do so I created, in the **build.gradle** file, a new task called backup, that has a type of Copy, as it can be acknowledge by the code below:

```Gradle
task backup(type: Copy) {
    from '/build/libs'
    include "demo-0.0.1-SNAPSHOT.jar"
    into '../dist'
}
```

In order to test the implementation of this new task i needed to type in the command line the following code:
```Bash
gradlew backup
```

**2 -**  Delete all the files generated by webpack (usually located at src/resources/main/static/built/). 
This new task should be executed automatically by gradle before the task clean. 

To do so I created, also in the **build.gradle** file, a new task called delete, that has a Delete type. This task has a 
dependency of the task clean, this means that the clean task will always run before the task of deleting the files 
generated by webpack.

As it can be acknowledge by the code below:

```Gradle
task delete(type: Delete) {
    dependsOn clean
    delete '/src/main/resources/static/built'
}
```

In order to test the implementation of this new task i needed to type in the command line the following code:
```Bash
gradlew delete
```

-----------------------------------------------------------------------------------------------------

## Analysis of an Alternative / Bazel

Bazel and Gradle can be classified as "Java Build" tools.

Bazel is an open-sourced part of the Blaze tool, which was designed to solve a huge problem that Google had in hands,
that was the need to build internal Google projects reliably and efficiently.

It has a multi-language support, due to the fact that Bazel supports not only Java, or C++ but can also be extended to support
another programming languages.

It has also a high-level build language, mainly due to the fact that the projects are described in 
the **BUILD** language, which is a concise text that describes a project as sets of small interconnected libraries, 
binaries and tests.

And has also a multi-platform support because the same tool and the same **BUILD** files can be used to build software for 
different architectures, for example. 

Google’s development environment, as we all know, is unusually large and complex, and with the help of Bazel 
they could have unusually strong guarantees regarding the integrity of their builds.

By this we can acknowledge that Bazel emphasizes the following features:

**Correctness -** Bazel builds were developed to always produce the correct output. For example, if two users call the 
same build at the same commit with the same Bazel flags on different machines, they will receive identical outcomes.
 
**Performance -** Builds are designed to execute as fast as possible having in consideration the resources available.
 
**Reproducibility-** Any instance of a build can be reproduced in any environment. For example, if a bug 
report says version XPTO of software YZX fails in production environment ZXY, a developer is capable of recreate it on 
their own machine with the confidence that they’re working on the same problem.

_____________________________________________________________________________________________________

###Implementation of the alternative using Bazel

Unfortunately i wasn't capable of implementing the alternative using Bazel, nevertheless, and regarding my research, 
there's a few steps needed in order to put Bazel up and running.

First of all we need to install it, to do so we need to go to [Bazel website](https://docs.bazel.build/versions/master/getting-started.html "Bazel Getting started page").

In order to install it we need to download a Bazel binary file, rename it to ``bazel.exe`` and add it to a directory.
This directory need to be added later to the PATH.

I noticed that we also need to create a **workspace** file as well a **build** file.

A **workspace** file which can be empty or contain references to externasl dependencies required to build the software that we want to build.

A **build** file is a short program that uses a language called Starlark which is a programming language
intended to be used  as a configuration language, as we can see through this [website](https://github.com/bazelbuild/starlark).
 
Long story short in my opinion the documentation available isn't clear regarding the steps that we need to take in 
order to solve some minor problems. Neither the documentation to implement a similar spring boot application
using Bazel.

Despite my failed attempt, it was a good experience in order to have some knowledge of different automation tools. 
 