package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Teste the constructor")
    public void testTheConstructor() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        //Act
        Employee wolverine = new Employee(firstName, lastName, description, jobTitle, emailField);
        //Assert

        assertEquals(firstName, wolverine.getFirstName());
        assertEquals(lastName, wolverine.getLastName());
        assertEquals(description, wolverine.getDescription());
        assertEquals(jobTitle, wolverine.getJobTitle());
        assertEquals(emailField, wolverine.getEmailField());
    }


    @Test
    @DisplayName("Teste the constructor null first name")
    public void testTheConstructorNullFirstName() {
        //Arrange
        String firstName = null;
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid firstname, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());

    }

    @Test
    @DisplayName("Teste the constructor null last name")
    public void testTheConstructorNullLastName() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = null;
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid lastname, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor null description")
    public void testTheConstructorNulldescription() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = null;
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid description, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor null job title")
    public void testTheConstructorNulljobTitle() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = null;
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid jobtitle, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor null email field")
    public void testTheConstructorNullEmailField() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = null;
        String message = "Employee to create hasn't a valid emailField, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor empty first name")
    public void testTheConstructorEmptyFirstName() {
        //Arrange
        String firstName = "";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid firstname, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor empty last name")
    public void testTheConstructorEmptyLastName() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid lastname, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor empty description")
    public void testTheConstructorEmptyDescription() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid description, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor empty job title")
    public void testTheConstructorEmptyJobTitle() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "";
        String emailField = "TransformerWolverine@marvel.com";
        String message = "Employee to create hasn't a valid jobtitle, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor empty email field")
    public void testTheConstructorEmptyEmailField() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "";
        String message = "Employee to create hasn't a valid emailField, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }

    @Test
    @DisplayName("Teste the constructor invalid email")
    public void testTheConstructorInvalidEmail() {
        //Arrange
        String firstName = "Wolverine";
        String lastName = "Transformer";
        String description = "Marvel Character";
        String jobTitle = "Taxi driver";
        String emailField = "TransformerWolverine_marvel.com";
        String message = "Employee to create hasn't a valid emailField, employee won't be created";
        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailField));
        assertEquals(message, thrown.getMessage());
    }


}